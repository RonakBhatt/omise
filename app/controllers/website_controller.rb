class WebsiteController < ApplicationController
  before_action :find_charity, only: [:donate]

  def index
    @token = nil
  end

  def donate
    # OmiseCustom::Payment class for create a charge located at "lib/omise_custom/payment.rb"
    request = OmiseCustom::Payment.new(@charity, params[:omise_token], (params[:amount].to_i * 100), (params[:amount].to_i != 999), 'THB', "Donation to #{@charity.name} [#{@charity.id}]", Rails.env ).create
    if request == true
      redirect_to root_path, notice:  notice
    else
      redirect_to root_path, alert: "Minimum amount can allowed 0.2."
    end
  end

  private

  # retrieve_token.
  def retrieve_token(token)
    if Rails.env.test?
      OpenStruct.new({
        id: "tokn_X",
        card: OpenStruct.new({
          name: "J DOE",
          last_digits: "4242",
          expiration_month: 10,
          expiration_year: 2020,
          security_code_check: false,
        }),
      })
    else
      Omise::Token.retrieve(token)
    end
  end

  #if Find Charity if not found than redirect to same page back with message.
  def find_charity
    @charity = Charity.find_by(id: params[:charity])
    if !@charity.present?
      failure_redirection
    end
  end

  #if something went wrong than it will redirect and show message.
  def failure_redirection
    @token = nil
    flash.now.alert = t(".failure")
    redirect_to root_path, alert: alert
  end

end
