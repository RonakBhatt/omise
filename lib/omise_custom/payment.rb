module OmiseCustom 
	class Payment
		extend ActiveModel::Naming
		include ActiveModel::Conversion
		include ActiveModel::Validations
		attr_reader :charity
		attr_reader :omise_token
		attr_reader :amount
		attr_reader :currency
		attr_reader :description
		attr_reader :environment

	  def initialize(charity, omise_token, amount, paid, currency, description, environment)
			@charity     = charity
			@omise_token = omise_token
			@amount      = amount
			@paid        = paid
			@currency    = currency
			@description = description
	    @environment = environment
	  end

	  def create
	  	return false if check_amount == false
	  	create_charge
	  end

	  private

	  def check_amount
	  	(amount.blank? || amount <= 20) ? false : true
	  end

	  def create_charge
	  	if environment == "test"
	  		test_charge
	  	else
	  		charge
	  	end
	  	check_charge
	  end

	  def test_charge
	  	charge = OpenStruct.new({ amount: amount, paid: paid})
	  end

	  def charge
	  	charge = Omise::Charge.create({ amount: amount, currency: currency, card: omise_token, description: description})
	  end

	  def check_charge
	  	if charge.paid?
        charity.credit_amount(charge.amount)
        flash.notice = t(".success")
        return true, notice
      else
      	raise charge.failure_code
      end
	  end
	end
end